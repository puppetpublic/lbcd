#
# This class handles adding and maintaining the lbcd daemon and package.
# There is one parameter: $nolbcd.  If left undef, nothing happens.  If set to
# true, then /etc/nolbcd is created; if set to false, then /etc/nolbcd is
# removed.

class lbcd (
  $nolbcd = undef
) {
  package { "lbcd": ensure => installed }

  base::iptables::rule { 'lbcd':
    description => 'Status queries from campus, Redwood City, and the Camino environment',
    source      => [ '171.64.0.0/14', '204.63.224.0/21', '10.123.20.0/24' ],
    protocol    => 'udp',
    port        => 4330,
  }

  # If nolbcd is not defined, then run lbcd and leave /etc/nolbcd alone
  # If nolbcd is defined, control /etc/nolbcd and lbcd service status
  if ($nolbcd == undef) {
    $lbcd_ensure = 'running'
  } else {
    if ($nolbcd) {
      $nolbcd_ensure = 'present'
      $lbcd_ensure   = 'stopped'
    } else {
      $nolbcd_ensure = 'absent'
      $lbcd_ensure   = 'running'
    }
    file { '/etc/nolbcd':
      ensure  => $nolbcd_ensure,
      content => '',
    }
  }

  # Either start the service, or leave it off.
  service { "lbcd":
    ensure    => $lbcd_ensure,
    hasstatus => false,
    status    => "pidof lbcd",
    require   => Package["lbcd"],
  }

  # Install default lbcd start option and daemon options file
  lbcd::default { "/etc/default/lbcd": ensure => present }

  # Install a syslog-filter
  file { "/etc/filter-syslog/lbcd":
    source  => "puppet:///modules/lbcd/filter-syslog";
  }

}

# This class no longer exists!
class lbcd::disabled {
  fail('lbcd::disabled no longer exists.  Instead, use the lbcd class and set the $nolbcd parameter to 1.')
}

# Default lbcd start option and daemon options.
define lbcd::default (
  $daemon_opts="-R -P /var/run/lbcd/lbcd.pid",
  $ensure,
) {
  case $ensure {
    present: {
      file { "$name":
        content => template("lbcd/lbcd.erb"),
        require => Package["lbcd"],
        notify  => Service["lbcd"];
      }
    }
    absent: {
      file { "$name": ensure => absent }
    }
    default: { crit ("Invalid ensure value: $ensure.") }
  }
}
